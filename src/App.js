import React, { Component } from "react";
import "sanitize.css";
import "./App.css";
import ContactList from "./Components/ContactsList";

let contacts = [
  {
    id: 1,
    name: "Scott",
    phone: "555 555 5555"
  },
  { id: 2, name: "Courtney", phone: "555 111 5555" },
  { id: 3, name: "Tim", phone: "555 111 5544" },
  { id: 4, name: "Bob", phone: "555 111 5443" }
];

export default class App extends Component {
  render() {
    return (
      <div>
        <h1>Contact List</h1>

        <ContactList contacts={contacts}></ContactList>
      </div>
    );
  }
}
