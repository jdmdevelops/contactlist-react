import React, { Component } from "react";
import Contact from "./Contact";

export default class ContactsList extends Component {
  constructor() {
    super();
    this.state = {
      search: "type here!"
    };
  }
  updateSearch(event) {
    this.setState({ search: event.target.value });
  }

  render() {
    let filteredContacts = this.props.contacts.filter(contact => {
      return (
        contact.name.toLowerCase().indexOf(this.state.search.toLowerCase()) !==
        -1
      );
    });
    return (
      <div>
        <ul>
          {filteredContacts.map(contact => (
            <Contact contact={contact} key={contact.id}></Contact>
          ))}
        </ul>
        <input
          type="text"
          value={this.state.search}
          onChange={this.updateSearch.bind(this)}
        />
      </div>
    );
  }
}
