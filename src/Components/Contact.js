import React, { Component } from "react";

export default class Contacts extends Component {
  render() {
    return (
      <li>
        {this.props.contact.name + " "}
        {this.props.contact.phone}
      </li>
    );
  }
}
